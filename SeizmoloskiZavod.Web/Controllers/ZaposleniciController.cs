﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class ZaposleniciController : Controller
    {
        // GET: Zaposlenici
        public ActionResult Index()
        {
            var uloge = Uloga.Get().ToList();
            ViewData["uloge"] = uloge;
            return View(Zaposlenik.Get());
        }

        // GET: Zaposlenici/Details/5
        public ActionResult Details(int id)
        {
            var obavijesti = Obavijest.Get().ToList();
            ViewData["obavijesti"] = obavijesti;
            ViewBag.IdTipovaList = new SelectList(TipObavijesti.Get(), "IdTipa", "NazTipa");

            return View(Zaposlenik.Get(id));
        }

        // GET: Zaposlenici/Create
        public ActionResult Create()
        {
            ViewBag.IdUlogeList = new SelectList(Uloga.Get(), "IdUloge", "NazUloge");
            return View(Zaposlenik.New());
        }

        // POST: Zaposlenici/Create
        [HttpPost]
        public ActionResult Create(int IdUlogeZaposlenika, string ImeZaposlenika, string PrezimeZaposlenika, string OIBZaposlenika, string KorisnickoImeZaposlenika, string LozinkaHashZaposlenika)
        {

            Zaposlenik zaposlenik = Zaposlenik.New();
            try
            { 
                zaposlenik.IdUlogeZaposlenika = IdUlogeZaposlenika;
                zaposlenik.ImeZaposlenika = ImeZaposlenika;
                zaposlenik.PrezimeZaposlenika = PrezimeZaposlenika;
                zaposlenik.OIBZaposlenika = OIBZaposlenika;
                zaposlenik.KorisnickoImeZaposlenika = KorisnickoImeZaposlenika;
                zaposlenik.LozinkaHashZaposlenika = LozinkaHashZaposlenika;
                zaposlenik = zaposlenik.Save();
                return RedirectToAction("Details", new { id = zaposlenik.IdZaposlenika });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (zaposlenik.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in zaposlenik.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(zaposlenik);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(zaposlenik);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(zaposlenik);
            }
        }

        // GET: Zaposlenici/Edit/5
        public ActionResult Edit(int id)
        {

            Zaposlenik zaposlenik = Zaposlenik.Get(id);
            ViewBag.IdUlogeList = new SelectList(Uloga.Get(), "IdUloge", "NazUloge");

            return View(zaposlenik);
        }

        // POST: Zaposlenici/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, int IdUlogeZaposlenika, string ImeZaposlenika, string PrezimeZaposlenika, string OIBZaposlenika, string KorisnickoImeZaposlenika, string LozinkaHashZaposlenika)
        {
            Zaposlenik zaposlenik = null;
            try
            {
                zaposlenik = Zaposlenik.Get(id);
                zaposlenik.IdUlogeZaposlenika = IdUlogeZaposlenika;
                zaposlenik.ImeZaposlenika = ImeZaposlenika;
                zaposlenik.PrezimeZaposlenika = PrezimeZaposlenika;
                zaposlenik.OIBZaposlenika = OIBZaposlenika;
                zaposlenik.KorisnickoImeZaposlenika = KorisnickoImeZaposlenika;
                zaposlenik.LozinkaHashZaposlenika = LozinkaHashZaposlenika;
                zaposlenik.Save();
                return RedirectToAction("Details", new { id = zaposlenik.IdZaposlenika });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (zaposlenik.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in zaposlenik.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(zaposlenik);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(zaposlenik);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(zaposlenik);
            }
        }

        // GET: Zaposlenici/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                Zaposlenik.Delete(id);
            }
            catch (Exception ex)
            {
                TempData["Pogreska"] = ex.Message;
                return RedirectToAction("/Details", new { id = id });
            }
            return RedirectToAction("Index");
        }
    }
}
