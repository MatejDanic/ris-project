﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Postaja : BusinessBase<Postaja>
    {
        private Postaja()
        {
        }

        private static PropertyInfo<int> IdPostajeProperty =
      RegisterProperty(typeof(Postaja), new PropertyInfo<int>(Reflector.GetPropertyName<Postaja>(x => x.IdPostaje)));
        public int IdPostaje
        {
            get { return GetProperty(IdPostajeProperty); }
            set { SetProperty(IdPostajeProperty, value); }
        }


        private static PropertyInfo<string> NazPostajeProperty =
      RegisterProperty(typeof(Postaja), new PropertyInfo<string>(Reflector.GetPropertyName<Postaja>(x => x.NazPostaje)));
        public string NazPostaje
        {
            get { return GetProperty(NazPostajeProperty); }
            set { SetProperty(NazPostajeProperty, value); }
        }

        private static PropertyInfo<string> MjestoPostajeProperty =
      RegisterProperty(typeof(Postaja), new PropertyInfo<string>(Reflector.GetPropertyName<Postaja>(x => x.MjestoPostaje)));
        public string MjestoPostaje
        {
            get { return GetProperty(MjestoPostajeProperty); }
            set { SetProperty(MjestoPostajeProperty, value); }
        }

        public static Postaja New()
        {
            return DataPortal.Create<Postaja>();
        }

        public static Postaja Get(int idPostaje)
        {
            return DataPortal.Fetch<Postaja>(new SingleCriteria<Postaja, int>(idPostaje));
        }

        public static List<Postaja> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from p in ctx.DataContext.Postaje
                       select new Postaja { IdPostaje = p.Id, NazPostaje = p.Naziv, MjestoPostaje = p.Mjesto };

                return data.ToList();
            }
                
        }
        public static void Delete(int idPostaje)
        {
            DataPortal.Delete(new SingleCriteria<Postaja, int>(idPostaje));
        }

        private void DataPortal_Fetch(SingleCriteria<Postaja, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Postaje.Find(criteria.Value);

                LoadProperty(IdPostajeProperty, data.Id);
                LoadProperty(NazPostajeProperty, data.Naziv);
                LoadProperty(MjestoPostajeProperty, data.Mjesto);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Postaja p = new Dal.Postaja
                {
                    Naziv = NazPostaje,
                    Mjesto = MjestoPostaje
                };

                ctx.DataContext.Postaje.Add(p);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdPostajeProperty, p.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Postaja p = ctx.DataContext.Postaje.Find(this.IdPostaje);

                p.Naziv = NazPostaje;
                p.Mjesto = MjestoPostaje;

                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Postaja, int>(ReadProperty(IdPostajeProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Postaja, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var p = ctx.DataContext.Postaje.Find(criteria.Value);
                if (p != null)
                {
                    ctx.DataContext.Postaje.Remove(p);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
