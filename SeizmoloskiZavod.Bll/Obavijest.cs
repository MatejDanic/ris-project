﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Obavijest : BusinessBase<Obavijest>
    {
        private Obavijest()
        {
        }

        private static PropertyInfo<int> IdObavijestiProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<int>(Reflector.GetPropertyName<Obavijest>(x => x.IdObavijesti)));
        public int IdObavijesti
        {
            get { return GetProperty(IdObavijestiProperty); }
            set { SetProperty(IdObavijestiProperty, value); }
        }

        private static PropertyInfo<int> IdTipaObavijestiProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<int>(Reflector.GetPropertyName<Obavijest>(x => x.IdTipaObavijesti)));
        public int IdTipaObavijesti
        {
            get { return GetProperty(IdTipaObavijestiProperty); }
            set { SetProperty(IdTipaObavijestiProperty, value); }
        }

        private static PropertyInfo<int?> IdZaposlenikaProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<int?>(Reflector.GetPropertyName<Obavijest>(x => x.IdZaposlenikaObavijesti)));
        public int? IdZaposlenikaObavijesti
        {
            get { return GetProperty(IdZaposlenikaProperty); }
            set { SetProperty(IdZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> NaslovObavijestiProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<string>(Reflector.GetPropertyName<Obavijest>(x => x.NaslovObavijesti)));
        public string NaslovObavijesti
        {
            get { return GetProperty(NaslovObavijestiProperty); }
            set { SetProperty(NaslovObavijestiProperty, value); }
        }

        private static PropertyInfo<string> SadrzajObavijestiProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<string>(Reflector.GetPropertyName<Obavijest>(x => x.SadrzajObavijesti)));
        public string SadrzajObavijesti
        {
            get { return GetProperty(SadrzajObavijestiProperty); }
            set { SetProperty(SadrzajObavijestiProperty, value); }
        }

        private static PropertyInfo<DateTime> DatumVrijemeProperty =
      RegisterProperty(typeof(Obavijest), new PropertyInfo<DateTime>(Reflector.GetPropertyName<Obavijest>(x => x.DatumVrijemeObavijesti)));
        public DateTime DatumVrijemeObavijesti
        {
            get { return GetProperty(DatumVrijemeProperty); }
            set { SetProperty(DatumVrijemeProperty, value); }
        }

        private static PropertyInfo<string> ZaposlenikObavijestiProperty =
     RegisterProperty(typeof(Obavijest), new PropertyInfo<string>(Reflector.GetPropertyName<Obavijest>(x => x.ZaposlenikObavijesti)));
        public string ZaposlenikObavijesti
        {
            get { return GetProperty(ZaposlenikObavijestiProperty); }
            set { SetProperty(ZaposlenikObavijestiProperty, value); }
        }

        private static PropertyInfo<string> TipObavijestiProperty =
     RegisterProperty(typeof(Obavijest), new PropertyInfo<string>(Reflector.GetPropertyName<Obavijest>(x => x.TipObavijestiNaziv)));
        public string TipObavijestiNaziv
        {
            get { return GetProperty(TipObavijestiProperty); }
            set { SetProperty(TipObavijestiProperty, value); }
        }

        public static Obavijest New()
        {
            return DataPortal.Create<Obavijest>();
        }

        public static Obavijest Get(int idObavijesti)
        {
            return DataPortal.Fetch<Obavijest>(new SingleCriteria<Obavijest, int>(idObavijesti));
        }

        public static List<Obavijest> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from o in ctx.DataContext.Obavijesti
                           select new Obavijest { IdObavijesti = o.Id, IdZaposlenikaObavijesti = o.IdZaposlenika, NaslovObavijesti = o.Naslov, SadrzajObavijesti = o.Sadrzaj, DatumVrijemeObavijesti = o.DatumVrijeme , IdTipaObavijesti = o.IdTipa};

                return data.ToList();
            }

        }
        public static void Delete(int idObavijesti)
        {
            DataPortal.Delete(new SingleCriteria<Obavijest, int>(idObavijesti));
        }

        private void DataPortal_Fetch(SingleCriteria<Obavijest, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Obavijesti.Find(criteria.Value);
                LoadProperty(IdZaposlenikaProperty, data.IdZaposlenika);
                LoadProperty(IdObavijestiProperty, data.Id);
                LoadProperty(NaslovObavijestiProperty, data.Naslov);
                LoadProperty(SadrzajObavijestiProperty, data.Sadrzaj);
                LoadProperty(DatumVrijemeProperty, data.DatumVrijeme);
                LoadProperty(IdTipaObavijestiProperty, data.IdTipa);
                LoadProperty(ZaposlenikObavijestiProperty, Zaposlenik.Get((int)data.IdZaposlenika).KorisnickoImeZaposlenika);
                LoadProperty(TipObavijestiProperty, TipObavijesti.Get((int)data.IdTipa).NazTipa);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Obavijest o = new Dal.Obavijest
                {
                    IdZaposlenika = IdZaposlenikaObavijesti,
                    Naslov = NaslovObavijesti,
                    Sadrzaj = SadrzajObavijesti,
                    IdTipa = IdTipaObavijesti,
                    DatumVrijeme = DateTime.Now
                };

                ctx.DataContext.Obavijesti.Add(o);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdObavijestiProperty, o.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Obavijest o = ctx.DataContext.Obavijesti.Find(this.IdObavijesti);

                o.IdZaposlenika = IdZaposlenikaObavijesti;
                o.Naslov = NaslovObavijesti;
                o.Sadrzaj = SadrzajObavijesti;
                o.IdTipa = IdTipaObavijesti;
                o.DatumVrijeme = DateTime.Now;

                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Obavijest, int>(ReadProperty(IdObavijestiProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Obavijest, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var o = ctx.DataContext.Obavijesti.Find(criteria.Value);
                if (o != null)
                {
                    ctx.DataContext.Obavijesti.Remove(o);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
