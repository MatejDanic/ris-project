﻿using System;
using System.Linq;
using Csla;
using Csla.Data;
using System.Collections.Generic;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class SeizmografInfoList : ReadOnlyListBase<SeizmografInfoList, SeizmografInfo>
    {
        private SeizmografInfoList()
        {
        }

        public static SeizmografInfoList Get()
        {
            return DataPortal.Fetch<SeizmografInfoList>();
        }
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                List<SeizmografInfo> data = new List<SeizmografInfo>();
                foreach (var s in ctx.DataContext.Seizmografi.AsNoTracking().ToList())
                {
                    data.Add(new SeizmografInfo(s.Id, s.Naziv));
                }

                IsReadOnly = false;
                this.AddRange(data);
                IsReadOnly = true;
            }
            RaiseListChangedEvents = true;
        }
    }
}