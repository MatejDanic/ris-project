﻿using System;
using System.Reflection;
using Csla;
using Csla.Serialization;

namespace SeizmoloskiZavod.Bll
{
    // Read-only informacije o osobi.
    [Serializable()]
    public class SeizmografInfo : ReadOnlyBase<SeizmografInfo>
    {
        internal SeizmografInfo(int id, string naziv)
        {
            IdSeizmografa = id;
            NazSeizmografa = naziv;
            IsSelected = false;
        }

        public int IdSeizmografa { get; private set; }
        public string NazSeizmografa { get; private set; }
        public bool IsSelected { get; private set; }

        public override string ToString()
        {
            return NazSeizmografa;
        }
    }
}