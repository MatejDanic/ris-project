﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class TipObavijesti : BusinessBase<TipObavijesti>
    {
        private TipObavijesti()
        {
        }

        private static PropertyInfo<int> IdTipaProperty =
      RegisterProperty(typeof(TipObavijesti), new PropertyInfo<int>(Reflector.GetPropertyName<TipObavijesti>(x => x.IdTipa)));
        public int IdTipa
        {
            get { return GetProperty(IdTipaProperty); }
            set { SetProperty(IdTipaProperty, value); }
        }


        private static PropertyInfo<string> NazTipaProperty =
      RegisterProperty(typeof(TipObavijesti), new PropertyInfo<string>(Reflector.GetPropertyName<TipObavijesti>(x => x.NazTipa)));
        public string NazTipa
        {
            get { return GetProperty(NazTipaProperty); }
            set { SetProperty(NazTipaProperty, value); }
        }


        public static TipObavijesti New()
        {
            return DataPortal.Create<TipObavijesti>();
        }

        public static TipObavijesti Get(int idTipa)
        {
            return DataPortal.Fetch<TipObavijesti>(new SingleCriteria<TipObavijesti, int>(idTipa));
        }

        public static List<TipObavijesti> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from t in ctx.DataContext.TipoviObavijesti
                           select new TipObavijesti { IdTipa = t.Id, NazTipa = t.Naziv };

                return data.ToList();
            }

        }
        public static void Delete(int idTipa)
        {
            DataPortal.Delete(new SingleCriteria<TipObavijesti, int>(idTipa));
        }

        private void DataPortal_Fetch(SingleCriteria<TipObavijesti, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.TipoviObavijesti.Find(criteria.Value);

                LoadProperty(IdTipaProperty, data.Id);
                LoadProperty(NazTipaProperty, data.Naziv);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.TipObavijesti t = new Dal.TipObavijesti
                {
                    Naziv = NazTipa
                };

                ctx.DataContext.TipoviObavijesti.Add(t);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdTipaProperty, t.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.TipObavijesti u = ctx.DataContext.TipoviObavijesti.Find(this.IdTipa);

                u.Naziv = NazTipa;

                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<TipObavijesti, int>(ReadProperty(IdTipaProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<TipObavijesti, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var t = ctx.DataContext.TipoviObavijesti.Find(criteria.Value);
                if (t != null)
                {
                    ctx.DataContext.TipoviObavijesti.Remove(t);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
